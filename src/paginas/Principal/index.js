import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import firebase from "../../Firebase";
import "../../estilos/estilos.css";





class Principal extends Component{

  constructor(props){
      super(props);
        this.state = {
        email: "",
        senha: "",
        nome: "",
        sobrenome: "",
        datanascimento: ""
        
    }

    
    
  }





  async componentDidMount(){
      await firebase.auth().onAuthStateChanged(async (usuario)=>{
          if(usuario){
              var uid = usuario.uid;

              await firebase.firestore().collection("usuario").doc(uid).get()
              .then((retorno)=>{
                
                  this.setState({
                      nome: retorno.data().nome,
                      sobrenome: retorno.data().sobrenome,
                      datanascimento: retorno.data().datanascimento
                  });
                  
              });
          }

      });
  }


  render(){
    return(
      <div> 
                <h1>Atividade Somativa 02 - DevOps</h1>
                <h2>Pipeline com Testes de Segurança e Envio de Resultado para o Telegram</h2>
                <h2>Sistema Web em React Integrado ao Firebase</h2>
                <h1>Tela de Principal</h1>
                <h2>Informações do Usuário Logado</h2>        
    
      <br/>

      Nome: {this.state.nome} <br/>
      Sobrenome: {this.state.sobrenome} <br/>
      Data de Nascimento: {this.state.datanascimento} <br/><br/>

      <Link to="/"><button>Voltar para Tela de Login</button></Link>
      
      </div>
    )
  }
} 

export default Principal;
