import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from '../../Firebase';
import "../../estilos/estilos.css";



class Cadastro extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            senha: "",
            nome: "",
            sobrenome: "",
            datanascimento: "",
            mensagem: ""
        }
        this.gravar = this.gravar.bind(this);
    }


    async gravar(){

        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then( async (retorno) => {

            await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                    nome: this.state.nome,
                    sobrenome: this.state.sobrenome,
                    datanascimento: this.state.datanascimento
                    
            });
            this.setState({mensagem:"Usuário Cadastrado com Sucesso"});
        });

    }

    render(){
        return(
            <div>
                <h1>Atividade Somativa 02 - DevOps</h1>
                <h2>Pipeline com Testes de Segurança e Envio de Resultado para o Telegram</h2>
                <h2>Sistema Web em React Integrado ao Firebase</h2>
                <h1>Tela de Cadastro</h1>
                <input type="email" placeholder='Email' onChange={(e)=>this.setState({email: e.target.value})} />
                <br/>
                <input type="password" placeholder='Senha' onChange={(e)=>this.setState({senha: e.target.value})} />
                <br/>
                <input  type="text" placeholder='Nome' onChange={(e)=>this.setState({nome: e.target.value})} />
                <br/>
                <input  type="text" placeholder='Sobrenome' onChange={(e)=>this.setState({sobrenome: e.target.value})} />
                <br/>
                <input  type="date" placeholder='Data de Nascimento' onChange={(e)=>this.setState({datanascimento: e.target.value})} />
                <br/>
                <button onClick={this.gravar}>Gravar</button> <br/> <br/>
                <h4>{this.state.mensagem}</h4><br/>
                <Link to="/"><button>Voltar para Tela de Login</button></Link>

                
            </div>
        )
    }
}



export default Cadastro;
