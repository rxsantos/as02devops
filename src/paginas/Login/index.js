import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from '../../Firebase';
import "../../estilos/estilos.css";

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            senha: "",
            mensagem: ""
        }

        this.acessar = this.acessar.bind(this);
    }

    async acessar(){

        await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
        .then(()=> {
            window.location.href = "./principal";
        })
        .catch((erro) => {
            this.setState({mensagem:"Usuário não Cadastrado"});
            console.log(erro);
        });
    }

    render(){
        return(
            <div>
                
                <h1>Atividade Somativa 02 - DevOps</h1>
                <h2>Pipeline com Testes de Segurança e Envio de Resultado para o Telegram</h2>
                <h2>Sistema Web em React Integrado ao Firebase</h2>
                <h1>Tela de Login</h1>
                <input type="email" placeholder='Email' onChange={(e)=>this.setState({email: e.target.value})} />
                <br/>
                <input type="password" placeholder='Senha' onChange={(e)=>this.setState({senha: e.target.value})} />
                <br/>
                
                <button onClick={this.acessar}>Acessar</button> <br/><br/> <br/> <br/>
                
                
                Clique no botão abaixo para realizar o cadastro.
                <h4>{this.state.mensagem}</h4><br/>
                <Link to="/cadastro"><button>Cadastrar Usuário</button></Link>
                
            </div>
        )
    }
}


export default Login;
