import React from 'react';
import { Link } from 'react-router-dom';
import "../../estilos/estilos.css";


function NotFound() {
  return(
      <div>
                <h1>Atividade Somativa 02 - DevOps</h1>
                <h2>Pipeline com Testes de Segurança e Envio de Resultado para o Telegram</h2>
                <h2>Sistema Web em React Integrado ao Firebase</h2>
                <h1>Página não Encontrada</h1>
          <Link to="/"><button>Ir para Tela de Login</button></Link>
      </div>
  )
}


export default NotFound;
