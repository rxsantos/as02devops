# Atividade Somativa 02 - DevOps - PUCPR

## Aplicação Web escrita em React para teste de segurança com OWAST e envio de resultado via Telegram

O deploy da aplicação será realizado na Oracle Cloud (OCI) em uma instância que executará o Docker.



## Bot do Telegram para visualizar resuldados dos Pull Requests
t.me/rxsantos_bot 


## Endereço do projeto na Oracle Cloud
Endereço da aplicação na Oracle Cloud: http://146.235.26.35:3000/


## Imagem Docker utilizada para o deploy do projeto
https://hub.docker.com/r/rxsantos/webas02

