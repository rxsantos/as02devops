FROM ubuntu:latest
WORKDIR /app
COPY . /app


RUN apt-get update -y && apt-get -y install curl gcc g++ make python3-distutils
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install nodejs -y
RUN npm install -g create-react-app 
RUN npm install firebase@8.9.1 react-router-dom
EXPOSE 3000
CMD npm start




#FROM selenium/standalone-chrome
#USER root
#RUN apt-get update && apt-get install python3-distutils -y
#RUN wget https://bootstrap.pypa.io/get-pip.py
#RUN python3 get-pip.py
#RUN pip install -r requirements.txt
#CMD python3 app.py



